# Основное:
**CTDatabase** - серверный модуль для приватного мода ClientTweaker (WindCheck) позволяющий сохранять ссылки сразу в базу данных

# Зависимости:
* Events https://gitlab.com/ctmodules/events
* ClientTweaker[2.0.0,)
* Включённый режим закачки на imgur в главном моде.

# Установка:
* Скачать мод с репозитория
* Закинуть мод в папку *mods*
* (При необходимости, если мод ставится на чистый forge) Создать в корне сервера папку willslib (для исключения путаницы) и положить библиотеку драйвера mysql в неё.
* Скопировать строку запуска от https://gitlab.com/ctmodules/ctdiscord#примеры-строк-запуска для forge.

# Настройка:
Конфиг мода будет находиться по пути config/ctdatabase.cfg \
Параметры:
 * enabled - Включён ли мод в данный момент.
 
 * host - jdbc:mysql://ip:port/базаданных к которому будет подключаться мод.
 
 * user - Пользователь.
 
 * pass - Пароль.
 
 * tableName - Название таблицы где будет всё храниться.
 
 * columnURL - Название колонки с сылками.
 
 * columnPlayer - Название колонки с никами игроков.
 
 * columnAdmin - Название колонки с никами Админов.
 
 * columnTime - Название колонки с временем.
 
# Клонирование репозитория:
 Выполнить сразу после клонирования проекта: git submodule update --init \
 
 Для обновления модуля Event: git submodule update --remote