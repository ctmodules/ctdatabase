package ru.will0376.ctmodules.ctdatabase;

import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import ru.will0376.ctmodules.events.EventLogPrint;
import ru.will0376.ctmodules.events.EventModStatusResponse;
import ru.will0376.ctmodules.events.utils.Logger;

@Mod(
		modid = Ctdatabase.MOD_ID,
		name = Ctdatabase.MOD_NAME,
		version = Ctdatabase.VERSION,
		serverSideOnly = true,
		acceptableRemoteVersions = "*",
		dependencies = "required-after:events@[1.0,)"


)
public class Ctdatabase {

	public static final String MOD_ID = "ctdatabase";
	public static final String MOD_NAME = "Ctdatabase";
	public static final String VERSION = "1.0";
	public static Config config;
	@Mod.Instance(MOD_ID)
	public static Ctdatabase INSTANCE;

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {
		config = new Config(event.getSuggestedConfigurationFile());
		config.launch();

	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		if (config.isEnabled()) {
			Logger.log(3, "[Main-DB-Starter]", "Checking DB");
			DataBase.checkTables();
		}
	}

	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {
		MinecraftForge.EVENT_BUS.post(new EventLogPrint(3, TextFormatting.GOLD + "[Ctdatabase]" + TextFormatting.RESET, "PostInit done!"));
		MinecraftForge.EVENT_BUS.post(new EventModStatusResponse(MOD_ID, config.isEnabled()));
	}
}