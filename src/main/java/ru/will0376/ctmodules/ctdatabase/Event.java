package ru.will0376.ctmodules.ctdatabase;

import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import ru.will0376.ctmodules.events.EventPrintAnswer;
import ru.will0376.ctmodules.events.EventReloadConfig;
import ru.will0376.ctmodules.events.EventRequestPrintScreenLog;
import ru.will0376.ctmodules.events.utils.ChatForm;
import ru.will0376.ctmodules.events.utils.Logger;
import ru.will0376.ctmodules.events.utils.Utils;

import java.util.ArrayList;

@Mod.EventBusSubscriber
public class Event {
	@SubscribeEvent
	public static void catchScreen(EventPrintAnswer e) {
		DataBase.addToLog(e.getPlayerNick(), e.getAdminNick(), e.getText(), System.currentTimeMillis() + "");
	}

	@SubscribeEvent
	public static void reload(EventReloadConfig e) {
		Ctdatabase.config.launch();
		if (Utils.getEPMP(e.getSender()) == null) Logger.log(1, "[Ctdatabase]", "Reloaded");
		else
			Utils.getEPMP(e.getSender()).sendMessage(new TextComponentString(TextFormatting.GOLD + "[Ctdatabase] Reloaded"));
	}

	@SubscribeEvent
	public static void catchLogs(EventRequestPrintScreenLog e) {
		if (Ctdatabase.config.isEnabled()) {
			ArrayList<String> list = DataBase.getFromLogForNick(e.getPlayer().getName());
			if (!list.isEmpty()) {
				ArrayList<String> tmplist = new ArrayList<>();
				list.stream().skip((e.getPage() - 1) * 5).forEach(a -> {
					if (list.indexOf(a) <= (e.getPage() - 1) * 5) tmplist.add(a);
				});
				if (e.getPlayer() == null)
					tmplist.forEach(System.out::println);
				else
					e.getPlayer().sendMessage(new TextComponentString(ChatForm.prefix + ":\n" + String.join("\n", tmplist)));
			}
		}
	}

}
